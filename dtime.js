/*
* https://gitlab.com/zlax/dtime-js
*
* REAL-TIME CLOCK (c) 2009 por Tiago Madeira (http://tiagomadeira.com/)
* Idealizado por Santaum (http://santaum.org/)
*
* All Hail Eris!
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function az(x) {
    if (x < 10)
        return "0"+x;
    return x;
}

function ly(Y) {
    return (Y % 4 == 0 && !(Y % 100 == 0 && Y % 400 != 0));
}

function dy(D, M, Y) {
    var dm = Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    if (ly(Y)) {
        dm[1] = 29;
    }
    var d = D;
    for (var i = 0; i < M-1; i++) {
        d+= dm[i]
    }
    return d;
}

function dtime(gregorianDate,resultar="default") {
    // Easter Island Winter Time Zone offset (-5*60*60*1000)
    var date = new Date(gregorianDate.getTime()-18000000);

    var D = date.getUTCDate();
    var M = date.getUTCMonth()+1;
    var Y = date.getUTCFullYear();
    var d = dy(D, M, Y);
    var sttiby = 0;
    if (ly(Y)) {
        if (d == 60) {
            sttiby = 1;
        } else if (d > 60) {
            d--;
        }
    }
    M = Math.floor(d/73);
    D = d % 73;
    if (D == 0) {
        M--;
        D = 73;
    }
    var seasonnum = M+1;
    switch (M) {
        case 0:
            M = "Chaos";
            break;
        case 1:
            M = "Discord";
            break;
        case 2:
            M = "Confusion";
            break;
        case 3:
            M = "Bureaucracy";
            break;
        case 4:
            M = "Aftermath";
            break;
        default:
            M = "fnord";
    }
    Y+= 1166;
    if (sttiby) {
        var ddatestr = "St. Tib's Day, "+Y+" YOLD";
    } else {
        var ddatestr = M+" "+az(D)+", "+Y+" YOLD";
    }
    var h = date.getUTCHours();
    var m = date.getUTCMinutes();
    var s = date.getUTCSeconds();
    var ms = date.getUTCMilliseconds();
    var e = h*3600000+m*60000+s*1000+ms;
    var ds = Math.round(e/864);
    h = Math.floor(ds/10000);
    ds%= 10000;
    m = Math.floor(ds/100);
    ds%= 100;
    s = ds;
    switch (resultar) {
        case "shortsec":
            return [ddatestr, h+":"+az(m), ":"+az(s)];
            break;
        case "ddatetime":
            return [Y, seasonnum, D, h, m, s];
            break;
        case "yold":
            return [Y];
            break;
        case "timeofyold":
            var dtimestr = seasonnum+"."+az(D)+" "+h+":"+az(m)+":"+az(s);
            return [dtimestr];
            break;
        default:
            var dtimestr = h+":"+az(m)+":"+az(s);
            return [ddatestr, dtimestr];
            break;
    }
}
