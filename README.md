Accurate astronomical Wheel of the Year

https://wheeloftheyear.soundragon.su

Automatic time zone adjustment: https://wheeloftheyear.soundragon.su/#auto

under DWTWL 2.55 license: https://soundragon.su/license/license.html

Time data from the Institute for Celestial Mechanics and Computation of Ephemerides

Vernal Equinox: https://www.imcce.fr/newsletter/docs/Equinoxe_printemps_1583_2999.pdf

Northern Solstice: https://www.imcce.fr/newsletter/docs/Solstice_ete_1583_2999.pdf

Autumnal Equinox: https://www.imcce.fr/newsletter/docs/Equinoxe_automne_1583_2999.pdf

Southern Solstice: https://www.imcce.fr/newsletter/docs/Solstice_hiver_1583_2999.pdf

Original Octosol image by DougInAMug: https://gitlab.com/DougInAMug/octosol

![solarwheeloftheyear](https://dev.ussr.win/zlax/solarwheeloftheyear/raw/branch/master/0.5.0_octosol.png)